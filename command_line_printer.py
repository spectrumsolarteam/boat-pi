import time
import json
from service import Service

# Goal: printing the storage values to the terminal
# not meant to use in production, but rather to be used during testing
class CommandLinePrinter(Service):
    def run(self):
        super().run()
        while True:
            try:
                print(self.storage.toJson())
                time.sleep(1)
            except:
                time.sleep(1)
                pass