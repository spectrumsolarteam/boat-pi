import os
import threading
import time
import logging
from datetime import datetime
from subprocess import check_call
from dotenv import load_dotenv
from store.store import Store

# Goal: this is the main file from which different threads are called.
# The Vessel class acts as a container for all storage, services, threads, etc.
# All parts/threads should be a subclass of Service
# All services must be added to the self.services list, so that they are accessible from any other service and can be shut down

# from command_line_printer import CommandLinePrinter
from http_sender import HttpSender
# from serial_transceiver import SerialTransceiver
# from mqtt_transceiver import MqttTransceiver
from canbus_receiver import CanbusReceiver
from gps_receiver import GpsReceiver
from local_saver import LocalSaver
from display_drawer import DisplayDrawer
from ups_receiver import UpsReceiver
from cpu_receiver import CpuReceiver
from hilink_receiver import HilinkReceiver
# from xsens_logger import XsensLogger

class Vessel:
    def __init__(self):
        self.storage = Store()
        
        # self.mqtt = MqttTransceiver(self)
        self.http = HttpSender(self)
        self.can0 = CanbusReceiver(self, bus='can0')
        self.can1 = CanbusReceiver(self, bus='can1')
        self.gps = GpsReceiver(self)
        self.local = LocalSaver(self)
        self.display1 = DisplayDrawer(self, bus=1)
        # self.display2 = DisplayDrawer(self)
        self.ups = UpsReceiver(self)
        self.cpu = CpuReceiver(self)
        self.hilink = HilinkReceiver(self)
        # self.xsenslogger = XsensLogger(self)

        self.services = [
            # self.mqtt,
            self.http,
            self.can0,
            self.can1,
            self.gps,
            self.local,
            self.display1,
            # self.display2,
            self.ups,
            self.cpu,
            self.hilink,
            # self.xsenslogger,
        ]

    def start(self):
        for service in self.services:
            y = threading.Thread(target=service.run)
            y.daemon = True
            y.start()

        try:
            while True:
                time.sleep(.1)
        except KeyboardInterrupt:
            self.killServices()
            print("Program ended. Threads have been killed.")

    def killServices(self):
        print("\n\nKilling all services... Wait a sec...")
        for service in self.services:
            try:
                service.kill()
            except:
                pass
        time.sleep(5)

    def shutdown(self):
        self.killServices()
        check_call(['sudo', 'poweroff'])

    def reboot(self):
        self.killServices()
        check_call(['sudo', 'reboot'])

    def update(self):
        check_call(['git', '-C', 'repo', 'reset', '--hard', 'main'])
        check_call(['cp', '/home/pi/repo/*.*', '/home/pi'])
        self.reboot()


if __name__ == '__main__':
    load_dotenv()

    logging.basicConfig(
        filename=os.getenv('LOGS_PATH')+'/'+datetime.now().strftime("%Y-%m-%d")+'.txt',
        format='%(asctime)s - %(message)s',
        level=logging.INFO
    )
    logging.info('Started!')

    vessel = Vessel()
    vessel.start()