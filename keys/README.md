This directory is expected to contain the following files:

* `google-roots.pem`; used in `mqtt_transceiver.py`
* `rsa.private`; used in `mqtt_transceiver.py`
* `rsa.public`; used in `mqtt_transceiver.py`

More information about the contents of these files (and how to create your own) will be added as soon as possible.