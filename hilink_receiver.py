import time
import logging
from service import Service
from huawei_lte_api.Client import Client
from huawei_lte_api.AuthorizedConnection import AuthorizedConnection

# Goal: get the status / network information from the USB modem (Huawei E3772)
# The modem acts as a small HTTP server with an API. Using the huawei_lte_api library, we can easliy read the connection status
# The login credentials for the modem are packed in the connect_url parameter, as well as the IP address
class HilinkReceiver(Service):
    connect_url = 'http://admin:admin@192.168.8.1/'

    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                connection = AuthorizedConnection(self.connect_url)
                hilink_client = Client(connection)
                time.sleep(1)

                while True:
                    if self.isStopped(): return
                    status = hilink_client.monitoring.status()
                    
                    connection_status = status['ConnectionStatus']
                    signal_level = status['SignalIcon']
                    network_type = status['CurrentNetworkType']

                    self.storage.set("hilink_connection_status", self.getConnectionStatus(connection_status))
                    self.storage.set("hilink_signal_level", signal_level)
                    self.storage.set("hilink_network_type", self.getNetworkType(network_type))

                    time.sleep(5)

            except Exception as ex:
                logging.info('Exception in HilinkReceiver. Retry in 5 seconds. Err: ' + str(ex))
                time.sleep(5)

    # map the connection status number to a human-readible status. Source: somewhere on the internet, seems to be correct.
    def getConnectionStatus(self, status):
        result = 'n/a'
        if status == '2' or status == '3' or status == '5' or status == '8' or status == '20' or status == '21' or status == '23' or status == '27' or status == '28' or status == '29' or status == '30' or status == '31' or status == '32' or status == '33':
            result = 'Connection failed, the profile is invalid'
        elif status == '7' or status == '11' or status == '14' or status == '37':
            result = 'Network access not allowed'
        elif status == '12' or status == '13':
            result = 'Connection failed, roaming not allowed'
        elif status == '201':
            result = 'Connection failed, bandwidth exceeded'
        elif status == '900':
            result = 'Connecting'
        elif status == '901':
            result = 'Connected'
        elif status == '902':
            result = 'Disconnected'
        elif status == '903':
            result = 'Disconnecting'
        elif status == '904':
            result = 'Connection failed or disabled'
        return result

    # map the network type number to a human-readible type. Source: somewhere on the internet, seems to be correct.
    def getNetworkType(self, type):
        result = 'n/a'
        if type == '0':
            result = 'No Service'
        elif type == '1':
            result = 'GSM'
        elif type == '2':
            result = 'GPRS (2.5G)'
        elif type == '3':
            result = 'EDGE (2.75G)'
        elif type == '4':
            result = 'WCDMA (3G)'
        elif type == '5':
            result = 'HSDPA (3G)'
        elif type == '6':
            result = 'HSUPA (3G)'
        elif type == '7':
            result = 'HSPA (3G)'
        elif type == '8':
            result = 'TD-SCDMA (3G)'
        elif type == '9':
            result = 'HSPA+ (4G)'
        elif type == '10':
            result = 'EV-DO rev. 0'
        elif type == '11':
            result = 'EV-DO rev. A'
        elif type == '12':
            result = 'EV-DO rev. B'
        elif type == '13':
            result = '1xRTT'
        elif type == '14':
            result = 'UMB'
        elif type == '15':
            result = '1xEVDV'
        elif type == '16':
            result = '3xRTT'
        elif type == '17':
            result = 'HSPA+ 64QAM'
        elif type == '18':
            result = 'HSPA+ MIMO'
        elif type == '19':
            result = 'LTE (4G)'
        elif type == '41':
            result = 'UMTS (3G)'
        elif type == '44':
            result = 'HSPA (3G)'
        elif type == '45':
            result = 'HSPA+ (3G)'
        elif type == '46':
            result = 'DC-HSPA+ (3G)'
        elif type == '64':
            result = 'HSPA (3G)'
        elif type == '65':
            result = 'HSPA+ (3G)'
        elif type == '101':
            result = 'LTE (4G)'
        return result