import math

# Goal: interpret the incoming canbus messages (coming in on canbus_receiver.py).
# for reach message (or 'sub-message' (please see the comments in canbus_receiver.py)), there exist a seperate method.
# For most methods, the bytes are interpreted as a integer, based on the documentation.
# Some messages contain only one value (which is the case for MG battery messages),
# some contain multiple values in one message (which is the case for MGM motor controller messages).
# Make sure to make them all static, so they can be called statically from canbus_receiver.py.

class CanbusMessageType:
    @staticmethod
    def batteryVoltage(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=False)
        storage.set('battery_voltage', val / 1000)

    @staticmethod
    def batteryCurrent(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=True)
        storage.set('battery_current', val / 100)

    """ Note! 'current charge' and 'current discharge' are swapped with respect to the protocol of MG """
    @staticmethod
    def batteryCurrentDischarge(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=True)
        storage.set('battery_current_discharge', val / 100)

    """ Note! 'current charge' and 'current discharge' are swapped with respect to the protocol of MG """
    @staticmethod
    def batteryCurrentCharge(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=True)
        storage.set('battery_current_charge', val / 100)

    @staticmethod
    def batterySOC(msg, storage):
        val = int.from_bytes(msg.data[4:5], byteorder='little', signed=False)
        storage.set('battery_soc', val)

    @staticmethod
    def batteryTimeToGo(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=False)
        storage.set('battery_time_to_go', val)

    @staticmethod
    def batteryCellTempHigh(msg, storage):
        val = int.from_bytes(msg.data[4:5], byteorder='little', signed=False)
        storage.set('battery_cell_temperature_high', val)

    @staticmethod
    def batteryCellTempLow(msg, storage):
        val = int.from_bytes(msg.data[4:5], byteorder='little', signed=False)
        storage.set('battery_cell_temperature_low', val)

    @staticmethod
    def batteryCellVoltageHigh(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=True)
        storage.set('battery_cell_voltage_high', val / 1000)

    @staticmethod
    def batteryCellVoltageLow(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=True)
        storage.set('battery_cell_voltage_low', val / 1000)

    @staticmethod
    def batteryBmsState(msg, storage):
        val = int.from_bytes(msg.data[4:], byteorder='little', signed=False)
        #storage.set('battery_bms_state', val)

    @staticmethod
    def batteryCellVoltage(msg, storage):
        val = int.from_bytes(msg.data[4:6], byteorder='little', signed=False)
        cellnr = int.from_bytes(msg.data[3:4], byteorder='little', signed=True)
        storage.set('battery_cell_' + str(cellnr) + '_voltage', val / 1000)

    @staticmethod
    def throttle(msg, storage):
        throttle_val = int.from_bytes(msg.data[0:1], byteorder='little', signed=True)        
        storage.set('throttle', throttle_val)

    @staticmethod
    def MGMPacket1(msg, storage):
        mgm_battery_voltage = int.from_bytes(msg.data[0:2], byteorder='little', signed=False) / 57.45
        mgm_battery_current = int.from_bytes(msg.data[2:4], byteorder='little', signed=False) / 10
        mgm_motor_rpm = int.from_bytes(msg.data[4:7], byteorder='little', signed=False) * 10

        storage.set('mgm_battery_voltage', mgm_battery_voltage)
        storage.set('mgm_battery_current', mgm_battery_current)
        storage.set('mgm_motor_rpm', 0 if mgm_motor_rpm < 50 else mgm_motor_rpm)
        
    @staticmethod
    def MGMPacket2(msg, storage):
        mgm_odometer = int.from_bytes(msg.data[0:4], byteorder='little', signed=False)
        mgm_controller_temperature = msg.data[4]
        mgm_motor_temperature = msg.data[5]
        # mgm_battery_temperature = msg.data[6]

        storage.set('mgm_odometer', mgm_odometer)
        storage.set('mgm_controller_temperature', mgm_controller_temperature)
        storage.set('mgm_motor_temperature', mgm_motor_temperature)
        # storage.set('mgm_battery_temperature', mgm_battery_temperature)
        
    @staticmethod
    def MGMPacket3(msg, storage):
        mgm_requested_output = int.from_bytes(msg.data[0:2], byteorder='little', signed=True) / 10
        mgm_real_output = int.from_bytes(msg.data[2:4], byteorder='little', signed=False) / 10
        # possibility: warning code number
        # possibility: failure code number
        
        storage.set('mgm_requested_output', mgm_requested_output)
        storage.set('mgm_real_output', mgm_real_output)
        
    @staticmethod
    def MGMPacket4(msg, storage):
        pass
        # mgm_remaining_battery_capacity = int.from_bytes(msg.data[0], byteorder='little', signed=False)
        # mgm_terminal_voltage = int.from_bytes(msg.data[6:8], byteorder='little', signed=False)

        # storage.set('mgm_requested_output', mgm_requested_output)
        # storage.set('mgm_real_output', mgm_real_output)

    @staticmethod
    def MpptCurrentVoltage(msg, storage):
        mpptnr = (msg.arbitration_id - 0x76C) // 2 + 1

        current = int.from_bytes(msg.data[0:4], byteorder='little', signed=False) / 1000
        voltage = int.from_bytes(msg.data[4:8], byteorder='little', signed=False) / 1000
        
        storage.set('mppt_' + str(mpptnr) + '_current', current)
        storage.set('mppt_' + str(mpptnr) + '_voltage', voltage)

    @staticmethod
    def MpptPowerTemperature(msg, storage):
        mpptnr = (msg.arbitration_id - 0x76C) // 2 + 1

        power = int.from_bytes(msg.data[0:4], byteorder='little', signed=False) / 1000
        temp = int.from_bytes(msg.data[4:6], byteorder='little', signed=False) / 100
        
        storage.set('mppt_' + str(mpptnr) + '_power', power)
        storage.set('mppt_' + str(mpptnr) + '_temp', temp)

    @staticmethod
    def XsensErrors(msg, storage):
        # power = int.from_bytes(msg.data[0:4], byteorder='little', signed=False) / 1000
        # temp = int.from_bytes(msg.data[4:6], byteorder='little', signed=False) / 100
        
        # storage.set('xsens_err_' + str(mpptnr) + '_power', power)
        pass

    @staticmethod
    def XsensEuler(msg, storage):
        roll = int.from_bytes(msg.data[0:2], byteorder='big', signed=True) * 0.0078
        pitch = int.from_bytes(msg.data[2:4], byteorder='big', signed=True) * 0.0078
        yaw = int.from_bytes(msg.data[4:6], byteorder='big', signed=True) * 0.0078
        
        storage.set('xsens_roll', roll)
        storage.set('xsens_pitch', pitch)
        storage.set('xsens_yaw', yaw)

    @staticmethod
    def XsensLatLon(msg, storage):
        lat = int.from_bytes(msg.data[0:4], byteorder='big', signed=True) * 5.9605e-08
        lon = int.from_bytes(msg.data[4:8], byteorder='big', signed=True) * 1.1921e-07
        
        storage.set('xsens_lat', lat)
        storage.set('xsens_lon', lon)

    @staticmethod
    def XsensAltitude(msg, storage):
        alt = int.from_bytes(msg.data[0:4], byteorder='big', signed=False) * 3.0518e-05
        
        storage.set('xsens_altitude', alt)

    @staticmethod
    def XsensVelocity(msg, storage):
        x = int.from_bytes(msg.data[0:2], byteorder='big', signed=True) * 0.0156 * 3.6
        y = int.from_bytes(msg.data[2:4], byteorder='big', signed=True) * 0.0156 * 3.6
        z = int.from_bytes(msg.data[4:6], byteorder='big', signed=True) * 0.0156 * 3.6

        xy = abs(math.sqrt(math.pow(x, 2) + math.pow(y, 2)))
        
        storage.set('xsens_velocity_kmh', xy)