import time
import logging
from subprocess import check_call

import serial
import serial.tools.list_ports
from pynmeagps import NMEAReader
from service import Service

DEVICE_PID = 0x23a3

# Goal: retrieve the current GPS coordinates and speed from a USB GPS receiver.
# Using the serial device directly
class GpsReceiver(Service):

    dateSet = False

    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                port = self.findPort()
                if port is None:
                    logging.info('No GPS device found. Retry in 10 seconds.')
                    time.sleep(10)
                    continue

                with serial.Serial(
                    port=port,\
                    baudrate=4800,\
                    parity=serial.PARITY_NONE,\
                    stopbits=serial.STOPBITS_ONE,\
                    bytesize=serial.EIGHTBITS,\
                        timeout=1) as ser:

                    nmr = NMEAReader(ser)

                    while True:
                        if self.isStopped(): return

                        # Get nmr packet
                        (raw_data, packet) = nmr.read()

                        if packet and packet.identity == "GNRMC":
                            if not self.dateSet and packet.date and packet.time:
                                cmd = f"sudo date -s \"{packet.date}T{packet.time}Z\""
                                check_call(cmd, shell=True)
                                self.dateSet = True

                            if packet.status == "A":
                                self.storage.set('gps_ser_lat', packet.lat)
                                self.storage.set('gps_ser_lon', packet.lon)
                                self.storage.set('gps_ser_track', packet.cog)
                                self.storage.set('gps_ser_speed', packet.spd*0.5144444 if packet.spd >= 0.25 else 0)

            except Exception as ex:
                logging.info('Exception in gps_receiver. Retry in 2 seconds. Ex: ' + str(ex))
                time.sleep(2)

    def findPort(self):
        ports = serial.tools.list_ports.comports()
        if len(ports) == 0: return None
        usb = list(filter(lambda p: p.pid == DEVICE_PID, ports))[0]
        return usb.device
