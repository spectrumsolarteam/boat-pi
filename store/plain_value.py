from store.store_value import StoreValue

class PlainValue(StoreValue):
    value = None

    def setValue(self, val):
        self.value = val

    def getValue(self):
        return self.value