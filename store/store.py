import json
import base64
import time

from store.plain_value import PlainValue
from store.moving_average import MovingAverage
from store.proxy_value import ProxyValue

# Goal: store values as soon as they come in, and provide a bundle of values once the storage is read.
# Every thread stores their values into this storage object. Once another thread want to save/upload/read the data, it can retrieve the most recent values of all threads at once
# There are multipe different "storage types", e.g. a moving average which hold a set of historic values.
# These special types are set in the dict creation below. If not specified, a PlainValue will be created automatically the first time a value is set to that key.
class Store:
    
    # these are the 'transceiver objects', which should be accessible from any thread. Inside these objects, there should be a connection variable inside.
    serial = None
    mqtt = None
    display = None

    def __init__(self):
        self.dict = {
            'battery_current': MovingAverage(historyLength=10),
            'battery_current_discharge': MovingAverage(historyLength=10),
            'battery_current_charge': MovingAverage(historyLength=10),

           'gps_speed': ProxyValue(self, ['xsens_speed', 'gps_ser_speed']),
           'gps_lat': ProxyValue(self, ['xsens_lat', 'gps_ser_lat']),
           'gps_lon': ProxyValue(self, ['xsens_lon', 'gps_ser_lon']),
        }

    def initValue(self, key):
        self.dict[key] = PlainValue()

    def set(self, key, val):
        if key not in self.dict:
            self.initValue(key)

        self.dict[key].set(val)

    def get(self, key):
        if key not in self.dict:
            return None

        return self.dict[key].get()

    def toJson(self, includeEpoch=False):
        result = dict()
        for key in self.dict.keys():
            result[key] = self.get(key)

        if includeEpoch:
            result['epoch'] = int(time.time())

        str = json.dumps(result)
        return str

    def getEncodedData(self):
        return base64.b64encode(self.toJson(includeEpoch=True).encode()).decode()

    def numberOfItems(self):
        return len(self.dict)
