from store.store_value import StoreValue

class ProxyValue(StoreValue):
    keys = None
    storage = None # type: Store

    def __init__(self, storage, keys):
        self.storage = storage
        self.keys = keys

    def setValue(self, val):
        pass

    def getValue(self):
        for key in self.keys:
            val = self.storage.get(key)
            if val:
                return val

        return None

    def valueIsOutdated(self):
        return False
