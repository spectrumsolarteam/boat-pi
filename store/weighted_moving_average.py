from store.moving_average import MovingAverage

class WeightedMovingAverage(MovingAverage):
    weights = []
    totalWeight = 0

    def __init__(self, historyLength = 10, weights = []):
        assert len(weights) == historyLength, "History length must be equal to length of weights"

        self.weights = weights
        self.totalWeight = sum(self.weights)
        super().__init__(historyLength=historyLength)

    def getValue(self):
        if len(self.history) < 1:
            return None

        return self.weightedSum() / self.totalWeight

    def weightedSum(self):
        s = 0
        for i in range(self.historyLength):
            s += (self.weights[i] * self.history.index(i))
        return s