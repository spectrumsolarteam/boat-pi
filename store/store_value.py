import time

# Base class for value types, e.g. PlainValue and MovingAverage. If the last update is longer than 6 seconds ago, because this value is considered outdated.
# The functions get() and set() are used use these classes, but internally getValue and setValue are used.
# Do not overrule get and set in child classes! Only overrule getValue() and setValue().
class StoreValue():
    epoch = 0

    def updateEpoch(self):
        self.epoch = time.time()

    def valueIsOutdated(self):
        return (self.epoch + 6) < time.time()

    def get(self):
        if self.valueIsOutdated():
            return None

        return self.getValue()

    def set(self, val):
        self.setValue(val)
        self.updateEpoch()

    # should be overruled by child class!
    def getValue(self):
        return None

    # should be overruled by child class!
    def setValue(self, val):
        pass
