import collections
from store.store_value import StoreValue

class MovingAverage(StoreValue):
    history = None

    def __init__(self, historyLength = 10):
        self.history = collections.deque([], maxlen=historyLength)

    def setValue(self, val):
        if self.valueIsOutdated():
            self.history.clear()
            
        self.history.append(val)

    def getValue(self):
        if len(self.history) < 1:
            return None

        return sum(self.history) / len(self.history)