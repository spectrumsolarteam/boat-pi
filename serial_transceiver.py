import serial
import logging
import io
import time
import re
from service import Service

# Goal: serial communication with "INDIGO", our own controller, which controls throttle and pumps
# There are two pumps and one throttle value. To trigger the device to send data, we have to send a message '1.0.0' to it
# Once sent, INDIGO will send back the values we want to know (throttle value and pump statuses)
# We can write data to remotely control the pumps, this is done by writing a "1" or "2" value at the right "index"
class SerialTransceiver(Service):
    connection = None

    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                ser = serial.Serial('/dev/ttyUSB_INDIGO', 9600, timeout=1) # note: ttyUSB_INDIGO is a symbolic link to a usb device. This symlink should be created (once) in advance.
                self.connection = ser
                ser.flush()
                ser.write(b'1.0.0') # following protocol: send this message to trigger the device to send data

                while True:
                    if self.isStopped(): return
                    if ser.in_waiting > 0:
                        line = ser.readline().decode('utf-8').rstrip()
                        
                        if re.match(r"[0|1],[0|1],[0-9]{1,3}$", line):
                            parameters = line.split(',')
                            
                            coolant_pump = True if int(parameters[0]) == 1 else False
                            bilge_pump = True if int(parameters[1]) == 1 else False
                            throttle_value = int(parameters[2])

                            self.storage.set('indigo_coolant_pump', coolant_pump)
                            self.storage.set('indigo_bilge_pump', bilge_pump)
                            self.storage.set('indigo_throttle_value', throttle_value)
                    
            except Exception as e:
                logging.info('Exception in serial tranceiver: ' + str(e))
                time.sleep(5)

    def setCoolantPump(self, val):
        ser = self.connection
        if ser is not None:
            setVal = b'1.1.0' if val == "true" else b'1.2.0'
            # ser.write(setVal) # temporarily disabled because it was freaking out, need fix!

    def setBilgePump(self, val):
        ser = self.connection
        if ser is not None:
            setVal = b'1.0.1' if val == "true" else b'1.0.2'
            # ser.write(setVal) # temporarily disabled because it was freaking out, need fix!