import time
import logging
from datetime import datetime
from service import Service
from IT8951.display import AutoEPDDisplay
# from IT8951.display import VirtualEPDDisplay
from IT8951.constants import DisplayModes
from PIL import Image, ImageDraw, ImageFont

# Goal: display values "realtime" on the e-ink (e-paper) display.
# this display thing makes use of the library: https://github.com/GregDMeyer/IT8951 to control the eink screen
# The page is partial updated with breaks of length set in update_interval parameter. The pagelayout is stored as a dictionary/list in the layout parameter.
# Althoug many drawer methods really look similiar, each parameter has a its own drawing method. This is to make it possible to make small deviations per value, as it is expected this class will increase significantly in the near future.
# On shutdown, the sceen has to be cleared because the values will stay on the screen forever if not clear/changed actively
class DisplayDrawer(Service):
    bus = None
    rotate = None

    updates_allowed = True
    update_interval = 0.5 # in seconds

    available_pages = [0]
    current_page = 2

    grid_size = 12

    def __init__(self, vessel, bus, rotate=None):
        super().__init__(vessel)

        self.bus = bus
        self.rotate = rotate

        self.layout = {
            # page 0
            0: [
                { "x": 0, "y": 0, "w": 4, "h": 2, "drawer": self._drawTitle, "font": 42, "text": 'Speed' },
                { "x": 0, "y": 2, "w": 4, "h": 3, "drawer": self._drawSpeed, "font": 72 },

                { "x": 4, "y": 0, "w": 4, "h": 2, "drawer": self._drawTitle, "font": 42, "text": 'Charge' },
                { "x": 4, "y": 2, "w": 4, "h": 3, "drawer": self._drawCharge, "font": 72 },

                { "x": 8, "y": 0, "w": 4, "h": 2, "drawer": self._drawTitle, "font": 42, "text": 'Discharge' },
                { "x": 8, "y": 2, "w": 4, "h": 3, "drawer": self._drawDischarge, "font": 72 },

                { "x": 0, "y": 5, "w": 4, "h": 2, "drawer": self._drawTitle, "font": 42, "text": 'SOC' },
                { "x": 0, "y": 7, "w": 4, "h": 3, "drawer": self._drawSoc, "font": 72 },

                { "x": 4, "y": 5, "w": 4, "h": 2, "drawer": self._drawTitle, "font": 42, "text": 'Lowest voltage' },
                { "x": 4, "y": 7, "w": 4, "h": 3, "drawer": self._drawLowestVoltage, "font": 72 },

                { "x": 8, "y": 5, "w": 4, "h": 2, "drawer": self._drawTitle, "font": 42, "text": 'RPM (x1000)' },
                { "x": 8, "y": 7, "w": 4, "h": 3, "drawer": self._drawRpm, "font": 72 },

                { "x": 0, "y": 10, "w": 12, "h": 2, "drawer": self._drawTime, "font": 36 },
            ],

            # page 1
            1: [                
                { "x": 1, "y": 0, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 1, "font": 42 },
                { "x": 1, "y": 1, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 2, "font": 42 },
                { "x": 1, "y": 2, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 3, "font": 42 },
                { "x": 1, "y": 3, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 4, "font": 42 },
                { "x": 1, "y": 4, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 5, "font": 42 },
                { "x": 1, "y": 5, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 6, "font": 42 },
                { "x": 1, "y": 6, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 7, "font": 42 },
                { "x": 1, "y": 7, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 8, "font": 42 },
                { "x": 1, "y": 8, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 9, "font": 42 },
                { "x": 1, "y": 9, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 10, "font": 42 },
               { "x": 1, "y": 10, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 11, "font": 42 },
               { "x": 1, "y": 11, "w": 1, "h": 1, "drawer": self._drawCellVoltage, "cellnr": 12, "font": 42 },


                { "x": 3, "y": 0, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Speed' },
                { "x": 3, "y": 1, "w": 3, "h": 2, "drawer": self._drawSpeed, "font": 72 },

                { "x": 6, "y": 0, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Throttle' },
                { "x": 6, "y": 1, "w": 3, "h": 2, "drawer": self._drawThrottle, "font": 72 },

                { "x": 9, "y": 0, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'RPM (x100)' },
                { "x": 9, "y": 1, "w": 3, "h": 2, "drawer": self._drawRpm, "font": 72 },


                { "x": 3, "y": 3, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Charge' },
                { "x": 3, "y": 4, "w": 3, "h": 2, "drawer": self._drawCharge, "font": 72 },

                { "x": 6, "y": 3, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Discharge' },
                { "x": 6, "y": 4, "w": 3, "h": 2, "drawer": self._drawDischarge, "font": 72 },

                { "x": 9, "y": 3, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Netto' },
                { "x": 9, "y": 4, "w": 3, "h": 2, "drawer": self._drawNettoPower, "font": 72 },


                { "x": 3, "y": 6, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'MC temp' },
                { "x": 3, "y": 7, "w": 3, "h": 2, "drawer": self._drawMGMTemp, "font": 72 },

                { "x": 6, "y": 6, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Battery temp' },
                { "x": 6, "y": 7, "w": 3, "h": 2, "drawer": self._drawBatteryTemp, "font": 72 },

                { "x": 9, "y": 6, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Battery voltage' },
                { "x": 9, "y": 7, "w": 3, "h": 2, "drawer": self._drawVoltage, "font": 72 },
                

                { "x": 3, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Battery perc' },
                { "x": 3, "y": 10, "w": 3, "h": 2, "drawer": self._drawSoc, "font": 72 },

                { "x": 6, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Time' },
                { "x": 6, "y": 10, "w": 3, "h": 0.7, "drawer": self._drawDate, "font": 42 },
                { "x": 6, "y": 10.7, "w": 3, "h": 0.7, "drawer": self._drawTime, "font": 42 },

                { "x": 9, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Modem' },
                { "x": 9, "y": 10, "w": 3, "h": 0.7, "drawer": self._drawHilinkConnection, "font": 42 },
                { "x": 9, "y": 10.7, "w": 3, "h": 0.7, "drawer": self._drawHilinkConnection2, "font": 42 },


                # { "x": 3, "y": 8, "w": 9, "h": 2, "drawer": self._drawHilinkConnection, "font": 40 },
                # { "x": 3, "y": 10, "w": 9, "h": 2, "drawer": self._drawTime, "font": 36 },
            ],

            # page 2
            2: [
                { "x": 0, "y": 0.3, "w": 2, "h": 0.7, "drawer": self._drawHilinkConnection2, "font": 40 },
                { "x": 2, "y": 0.3, "w": 2, "h": 0.7, "drawer": self._drawHilinkConnection, "font": 40 },

                { "x": 4, "y": 0.3, "w": 3, "h": 0.7, "drawer": self._drawLogo, "font": 40 },

                { "x": 8.1, "y": 0.3, "w": 3.9, "h": 0.7, "drawer": self._drawDateAndTime, "font": 40 },


                { "x": 0, "y": 2, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Speed' },
                { "x": 0, "y": 3, "w": 3, "h": 1.5, "drawer": self._drawSpeed, "font": 72 },

                { "x": 2.5, "y": 2, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Throttle' },
                { "x": 2.5, "y": 3, "w": 3, "h": 1.5, "drawer": self._drawThrottle, "font": 72 },

                { "x": 5, "y": 2, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'RPM (x100)' },
                { "x": 5, "y": 3, "w": 3, "h": 1.5, "drawer": self._drawRpm, "font": 72 },

                { "x": 7.5, "y": 2, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'SOC (%)' },
                { "x": 7.5, "y": 3, "w": 3, "h": 1.5, "drawer": self._drawSoc, "font": 72 },


                { "x": 0, "y": 5.5, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Charge' },
                { "x": 0, "y": 6.5, "w": 3, "h": 1.5, "drawer": self._drawCharge, "font": 72 },

                { "x": 2.5, "y": 5.5, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Discharge' },
                { "x": 2.5, "y": 6.5, "w": 3, "h": 1.5, "drawer": self._drawDischarge, "font": 72 },

                { "x": 5, "y": 5.5, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Netto' },
                { "x": 5, "y": 6.5, "w": 3, "h": 1.5, "drawer": self._drawNettoPower, "font": 72 },

                { "x": 7.5, "y": 5.5, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'MC temp' },
                { "x": 7.5, "y": 6.5, "w": 3, "h": 1.5, "drawer": self._drawMGMTemp, "font": 72 },


                { "x": 0, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Battery voltage' },
                { "x": 0, "y": 10, "w": 3, "h": 1.5, "drawer": self._drawVoltage, "font": 72 },

                { "x": 2.5, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Lowest cell' },
                { "x": 2.5, "y": 10, "w": 3, "h": 1.5, "drawer": self._drawLowestVoltage, "font": 72 },

                { "x": 5, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": ' ' },
                { "x": 5, "y": 10, "w": 3, "h": 1.5, "drawer": self._drawTitle, "font": 72, "text": "?" },

                { "x": 7.5, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Battery temp' },
                { "x": 7.5, "y": 10, "w": 3, "h": 1.5, "drawer": self._drawBatteryTemp, "font": 72 },

                {"x": 10, "y": 1, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 1},
                {"x": 10, "y": 2, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 2},
                {"x": 10, "y": 3, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 3},
                {"x": 10, "y": 4, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 4},
                {"x": 10, "y": 5, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 5},
                {"x": 10, "y": 6, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 6},
                {"x": 10, "y": 7, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 7},
                {"x": 10, "y": 8, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 8},
                {"x": 10, "y": 9, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 9},
                {"x": 10, "y": 10, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 10},
                {"x": 10, "y": 11, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 11},
                {"x": 10, "y": 12, "w": 2, "h": 1, "drawer": self._drawMpptPower, "font": 38, "mppt_nr": 12},



                # { "x": 6, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Time' },
                # { "x": 6, "y": 10, "w": 3, "h": 0.7, "drawer": self._drawDate, "font": 42 },
                # { "x": 6, "y": 10.7, "w": 3, "h": 0.7, "drawer": self._drawTime, "font": 42 },

                # { "x": 9, "y": 9, "w": 3, "h": 1, "drawer": self._drawTitle, "font": 42, "text": 'Modem' },
                # { "x": 9, "y": 10, "w": 3, "h": 0.7, "drawer": self._drawHilinkConnection, "font": 42 },
                # { "x": 9, "y": 10.7, "w": 3, "h": 0.7, "drawer": self._drawHilinkConnection2, "font": 42 },


                # { "x": 3, "y": 8, "w": 9, "h": 2, "drawer": self._drawHilinkConnection, "font": 40 },
                # { "x": 3, "y": 10, "w": 9, "h": 2, "drawer": self._drawTime, "font": 36 },
            ]
        }

    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                self.display = AutoEPDDisplay(vcom=-2.06, spi_hz=24000000, bus=self.bus, rotate=self.rotate) # real display
                # self.display = VirtualEPDDisplay() # virtual display
                time.sleep(0.1)
                self.display.clear()

                while True:
                    if self.isStopped(): return
                    if self.updates_allowed:
                        # print("Updating...")
                        self.updateValues()
                    time.sleep(self.update_interval)

            except Exception as ex:
                logging.info('Exception in DisplayDrawer. Retry in 5 seconds. Err: ' + str(ex))
                time.sleep(5)

    def kill(self):
        super().kill()
        self.clearScreen()

    def updateValues(self):
        try:
            for pageItem in self.layout[self.current_page]:
                try:
                    if(callable(pageItem.get('drawer'))):
                        pageItem.get('drawer')(pageItem)
                except:
                    pass

            self.display.draw_full(DisplayModes.DU)
        except Exception as ex:
            logging.info('Exception in DisplayDrawer (updateValues). Err: ' + str(ex))

    def clearScreen(self):
        self.updates_allowed = False
        time.sleep(self.update_interval)
        try:
            self.display.clear()
        except:
            pass

    def _resetBox(self, x, y, w, h):
        img_width, img_height = self.display.frame_buf.size
        self.display.frame_buf.paste(0xff, box=(
                    round(img_width/self.grid_size*x),
                    round(img_height/self.grid_size*y),
                    round(img_width/self.grid_size*x) + round(img_width/self.grid_size*w),
                    round(img_height/self.grid_size*y) + round(img_height/self.grid_size*h)
        ))

    def _placeText(self, x, y, w, h, fsize, text, bold=False):
        fontsize = fsize
        img = self.display.frame_buf
        draw = ImageDraw.Draw(img)
        self._resetBox(x, y, w, h)

        try:
            if bold:
                font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeSansBold.ttf', fontsize)
            else:
                font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeSans.ttf', fontsize)
        except OSError:
            font = ImageFont.truetype('/usr/share/fonts/TTF/DejaVuSans.ttf', fontsize)

        img_width, img_height = img.size
        text_width, _ = font.getsize(text)
        text_height = fontsize

        box_w = img_width/self.grid_size
        box_h = img_height/self.grid_size
        draw_x = round(((box_w) * x) + (box_w/2 * w) - (text_width/2))
        draw_y = round(((box_h) * y) + (box_h/2 * h) - (text_height/2))

        draw.text((draw_x, draw_y), text, align='center', font=font)

    def _drawLogo(self, layoutdef):
        # TODO: clean this mess and fix logo colors not inverted (black background with white letters)
        img = self.display.frame_buf
        logo_path = '/home/pi/assets/logo.png'

        img_width, img_height = img.size
        paste_coords = [int(img_width/2)-250, 5]
        logo = Image.open(logo_path)

        # self._resetBox(paste_coords[0], paste_coords[1], 4, 1)
        # clearing image to white
        img.paste(0xFF, box=(paste_coords[0], paste_coords[1], 500, 140))

        dims = (500, 140)

        logo.thumbnail(dims)
        img.paste(logo, paste_coords)

        self.display.draw(DisplayModes.GC16)

    def _drawTitle(self, layoutdef):
        text = '--'
        try:
            text = str(layoutdef.get('text'))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text)

    def _drawDateAndTime(self, layoutdef):
        text = '--'
        try:
            text = datetime.now().strftime("%d-%m-%Y %H:%M")
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text)

    def _drawDate(self, layoutdef):
        text = '--'
        try:
            text = datetime.now().strftime("%d-%m-%Y")
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text)

    def _drawTime(self, layoutdef):
        text = '--'
        try:
            text = datetime.now().strftime("%H:%M")
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text)

    def _drawSpeed(self, layoutdef):
        text = '--'
        try:
            value = self.storage.get("gps_speed")
            if value is not None:
                text = '{:.1f} km/h'.format(value*3.6)
        except Exception as ex:
            logging.info("Exception in drawSpeed; " + str(ex))
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawThrottle(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("mgm_requested_output") is not None:
                text = '{:.0f} %'.format(self.storage.get("mgm_requested_output"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawVoltage(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_voltage") is not None:
                text = '{:.1f} V'.format(self.storage.get("battery_voltage"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawCharge(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_current_charge") is not None and self.storage.get("battery_voltage") is not None:
                value = self.storage.get("battery_current_charge") * self.storage.get("battery_voltage")
                if value < 150:
                    text = '{:.1f} W'.format(value)
                elif value < 1000:
                    text = '{:.0f} W'.format(round(value, -1))
                else:
                    text = '{:.2f} kW'.format(value/1000)
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawDischarge(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_current_discharge") is not None and self.storage.get("battery_voltage") is not None:
                value = self.storage.get("battery_current_discharge") * self.storage.get("battery_voltage")
                if value < 150:
                    text = '{:.1f} W'.format(value)
                elif value < 1000:
                    text = '{:.0f} W'.format(round(value, -1))
                else:
                    text = '{:.2f} kW'.format(value/1000)
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawNettoPower(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_current") is not None and self.storage.get("battery_voltage") is not None:
                value = self.storage.get("battery_current") * self.storage.get("battery_voltage")
                if value < 150:
                    text = '{:.1f} W'.format(value)
                elif value < 1000:
                    text = '{:.0f} W'.format(round(value, -1))
                else:
                    text = '{:.2f} kW'.format(value/1000)
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawSoc(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_soc") is not None:
                text = '{:.1f}%'.format(self.storage.get("battery_soc"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawCellVoltage(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_cell_" + str(layoutdef.get('cellnr')) + "_voltage") is not None:
                text = '{}: {:.2f} V'.format(layoutdef.get('cellnr'), self.storage.get("battery_cell_" + str(layoutdef.get('cellnr')) + "_voltage"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text)

    def _drawLowestVoltage(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_cell_voltage_low") is not None:
                text = '{:.2f} V'.format(self.storage.get("battery_cell_voltage_low"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawRpm(self, layoutdef):
        text = '--'
        try:
            value = self.storage.get("mgm_motor_rpm")
            if value is not None:
                text = '{:.1f}'.format(value / 100)
        except Exception as ex:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)
    
    def _drawMGMTemp(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("mgm_controller_temperature") is not None:
                text = '{:.1f} °C'.format(self.storage.get("mgm_controller_temperature"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)
            
    def _drawBatteryTemp(self, layoutdef):
        text = '--'
        try:
            if self.storage.get("battery_cell_temperature_high") is not None:
                text = '{:.1f} °C'.format(self.storage.get("battery_cell_temperature_high"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text, bold=True)

    def _drawMpptPower(self, layoutdef):
        text = '--'
        try:
            mppt_nr = layoutdef.get('mppt_nr')
            if self.storage.get(f"mppt_{mppt_nr}_power") is not None:
                text = '{}: {:.1f}W'.format(mppt_nr, self.storage.get(f"mppt_{mppt_nr}_power"))
        except:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'),
                        layoutdef.get('font'), text, bold=False)

    def _drawHilinkConnection(self, layoutdef):
        text = '--'
        try:
            connection_status = self.storage.get("hilink_connection_status")
            if connection_status is not None:
                if(connection_status == 'Connected'):
                    text = self.storage.get("hilink_network_type")
                else:
                    text = connection_status
        except Exception as ex:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text)

    def _drawHilinkConnection2(self, layoutdef):
        text = '--'
        signal_icons = {
            '0': u'\u2581' + u'\u2581' + u'\u2581' + u'\u2581' + u'\u2581',
            '1': u'\u2581' + u'\u2581' + u'\u2581' + u'\u2581' + u'\u2581',
            '2': u'\u2581' + u'\u2583' + u'\u2581' + u'\u2581' + u'\u2581',
            '3': u'\u2581' + u'\u2583' + u'\u2584' + u'\u2581' + u'\u2581',
            '4': u'\u2581' + u'\u2583' + u'\u2584' + u'\u2586' + u'\u2581',
            '5': u'\u2581' + u'\u2583' + u'\u2584' + u'\u2586' + u'\u2588'
        }
        try:
            connection_status = self.storage.get("hilink_connection_status")
            if connection_status is not None:
                if(connection_status == 'Connected'):
                    text = signal_icons.get(self.storage.get("hilink_signal_level"))
        except Exception as ex:
            pass
        self._placeText(layoutdef.get('x'), layoutdef.get('y'), layoutdef.get('w'), layoutdef.get('h'), layoutdef.get('font'), text)
