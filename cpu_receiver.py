import time
import logging
from gpiozero import LoadAverage, CPUTemperature
from service import Service

# Goal: gather information about the Pi's CPU usage and temperature
# Simple script using a library to get this information.
class CpuReceiver(Service):
    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                temp = CPUTemperature()
                la = LoadAverage(minutes=1)

                # print('Temp: {}C'.format(temp.temperature))
                # print('CPU: {}%'.format(la.value*100))

                self.storage.set("rpi_temperature", round(temp.temperature*10)/10) # in deg C
                self.storage.set("rpi_cpu", round(la.value * 100)) # in %

                time.sleep(10)

            except Exception as ex:
                # print('Exception in UPS thread (B). Retrying in 5 seconds...', ex)
                logging.info('Exception in UPS thread. Retrying in 5 seconds... ' + str(ex))
                time.sleep(5)