import os
import time
import logging
from datetime import datetime
from service import Service

# Goal: save xsens data in higher frequency compared to local_saver.
# Everything is saved in the folder "datalogs". For each day, a new folder is created.
# To make sure the files don't become too large and stay manageable, every two hours of the day a new file is created.
class XsensLogger(Service):
    data: list[str] = []

    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                for i in range(10):
                    self.addToDataBuffer(','.join([
                        round(time.time(), 3),
                        round(self.storage.get('xsens_speed')*3.6, 3),
                        round(self.storage.get('xsens_roll'), 3),
                        round(self.storage.get('xsens_pitch'), 3),
                        round(self.storage.get('xsens_yaw'), 3),
                    ]))
                    time.sleep(0.2)

                self.saveData()
            except Exception as e:
                logging.info('Exception in xsens_logger: ' + str(e))
                time.sleep(5)
                pass

    def addToDataBuffer(self, row: str):
        self.data.append(row)

    def clearDataBuffer(self):
        self.data.clear()

    def saveData(self):
        if len(self.data) == 0:
            return

        filename = self.getFileName()
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        f = open(filename, "a+")
        f.writelines(self.data)
        f.close()

        self.clearDataBuffer()

    def getFileName(self):
        now = datetime.now()
        folder = now.strftime("%Y-%m-%d")
        if((now.hour % 2) == 0):
            file = now.replace(microsecond=0, second=0, minute=0).strftime("%H")
        else:
            file = now.replace(microsecond=0, second=0, minute=0).strftime("%H")
            
        return os.getenv('DATALOGS_PATH') + '/' + folder + "/xsens_" + file + "h.json"
