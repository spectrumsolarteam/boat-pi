# Spectrum Solarteam Boat Raspberry Pi software
## README
This is the software used by Spectrum Solarteam to collect all kinds of data from the boat. It sends this data to the internet and displays the data on a display.

## WIP
This project is an ongoing work in progress. We will try to have this repository, especially the main branch, as complete and stable as possible. If that's not the case, we will try to mention all special cases below:

* The repository does not yet include the library or steps to install the library required for `display_drawer.py`. To run without errors, https://github.com/GregDMeyer/IT8951 needs to be installed in a specific directory.
* A setup script is not yet provided, but will be added soon.
* No overview of settings on the Pi and, more important, list of additional hardware are provided. These will be added as soon as possible.

However, we're happy to help you if, so shoot an email to info@spectrumsolarteam.nl for more info about this.

## Documentation
Sure, documentation is important. We try our best ;-) A (brief, but hopefully extensive enough) documentation can be found at the top of each file. The main file, to run the whole process, is `vessel.py`. The project is aimed to be run on Python 3.

## Hardware
Almost all parts are based on specific hardware to make things work. A list:
* Waveshare RS485 CAN HAT - https://www.waveshare.com/wiki/RS485_CAN_HAT (implemented in: `canbus_receiver.py`)
* Waveshare 6inch HD e-Paper HAT - https://www.waveshare.com/wiki/6inch_HD_e-Paper_HAT (implemented in: `display_drawer.py`)
* Waveshare UPS HAT - https://www.waveshare.com/wiki/UPS_HAT (implemented in: `ups_receiver.py`)

## Libraries
Several libraries are used. All libraries downloaded using Pip can be found in `requirements.txt`.
One special library is the one used in `display_drawer.py`, which can be found at https://github.com/GregDMeyer/IT8951. This needs to be installed in the folder `IT8951`. More information about the installation will follow soon.

## Questions
If you have any questions, feel free to ask: info@spectrumsolarteam.nl

## License
This project is licensed under GNU GPLv3, because we would like you to share your modifications with the world, especially the solar boat community.