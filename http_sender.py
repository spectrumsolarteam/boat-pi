import time
import requests
import json
import logging
from service import Service

# Goal: send the data known to the server, over HTTP.
# It makes an HTTP request every second. Timeout is set to 0.6 seconds.
# In practice, requests which take longer than .6s will timeout on the Pi, but they will actually make it to the server. Only the response is ignored.
# The code makes sure the request is made every second, because it calculates the timeout based on the time the request took.
class HttpSender(Service):
    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                t = time.time()
                self.postDataToCetacea()
                sleeptime = 1 - (time.time() - t)
                time.sleep(0 if sleeptime < 0 else sleeptime)
                # time.sleep(1)
            except IOError as e:
                logging.error('IO Error in HttpSender: ' + str(e))
                pass
            except Exception as e:
                logging.error('Exception in HttpSender: ' + str(e))
                time.sleep(10)
                pass
            except:
                logging.error('Unknown except in postDataToCetacea')
                time.sleep(10)
                pass


    def postDataToCetacea(self):
        payload = {
            "message": {
                "attributes": {
                    "deviceId": "spectrum-apparaat"
                }
            }
        }
        payload['message']['data'] = self.storage.getEncodedData()
        try:
            r = requests.post('https://api.cetacea.solar/iot/logs', json=payload, timeout=1.5)
        except requests.exceptions.ReadTimeout as e:
            logging.error('ReadTimeout in postDataToCetacea: ' + str(e))
            pass
        except Exception as e:
            logging.error('Exception in postDataToCetacea: ' + str(e))
            time.sleep(10)
            pass
        except:
            logging.error('Unknown except in postDataToCetacea')
            time.sleep(10)
            pass