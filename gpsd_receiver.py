import time
import logging
import gpsd
from service import Service
from numbers import Number

# Goal: retrieve the current GPS coordinates and speed from a USB GPS receiver.
# The famous GPSD service is running in the background.
# As there are many GPSD libraries for Python out there, this is the one we got it working with on Python 3: https://github.com/MartijnBraam/gpsd-py3
class GpsdReceiver(Service):
    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                gpsd.connect()
                while True:
                    if self.isStopped(): return
                    try:
                        packet = gpsd.get_current()
                        self.storage.set('gpsd_lat', self.zeroIfNotANumber(packet.lat))
                        self.storage.set('gpsd_lon', self.zeroIfNotANumber(packet.lon))
                        self.storage.set('gpsd_track', self.zeroIfNotANumber(packet.track))
                        if self.zeroIfNotANumber(packet.speed()) < 0.25:
                            self.storage.set('gpsd_speed', 0)
                        else:
                            self.storage.set('gpsd_speed', self.zeroIfNotANumber(packet.speed()))
                        time.sleep(0.25)
                    except UserWarning:
                        time.sleep(1)
            except Exception as ex:
                logging.info('Exception in gps_receiver. Retry in 2 seconds. Ex: ' + str(ex))
                time.sleep(2)

    def zeroIfNotANumber(self, v):
        if isinstance(v, Number):
            return v
        else:
            return 0