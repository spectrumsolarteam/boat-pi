import os
import time
import logging
from datetime import datetime
from service import Service

# Goal: save the data to local files.
# When there's no internet available, this can serve as a backup option, and synchronize with the database later on.
# Data is saved as a JSON represenatation, with on each line a new entry.
# Everything is saved in the folder "datalogs". For each day, a new folder is created.
# To make sure the files don't become too large and stay manageable, every two hours of the day a new file is created.
class LocalSaver(Service):
    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                self.saveData()
                time.sleep(1)
            except Exception as e:
                logging.info('Exception in local_saver: ' + str(e))
                time.sleep(5)
                pass

    def saveData(self):
        filename = self.getFileName()
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        f = open(filename, "a+")
        f.write(self.storage.toJson(includeEpoch = True) + "\n")
        f.close()

    def getFileName(self):
        now = datetime.now()
        folder = now.strftime("%Y-%m-%d")
        if((now.hour % 2) == 0):
            file = now.replace(microsecond=0, second=0, minute=0).strftime("%H")
        else:
            file = now.replace(microsecond=0, second=0, minute=0).strftime("%H")
            
        return os.getenv('DATALOGS_PATH') + '/' + folder + '/' + file + 'h.json'
