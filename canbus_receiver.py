import os
import can
import time
import logging
from subprocess import check_call
from message_type import CanbusMessageType
from service import Service

# Goal: receive canbus messages from the canbus network.
# We use a Waveshare canbus hat.
# Using the dispatchDict, the right processor for the message is chosen based on the arbitration id (a.k.a. can_id).
# Dispatching can either be based on only arbitration id (one level deep),
# or (because of the MG Electronics battery) also based on the 2nd (index 1), 3rd (index 2) and 4th data (index 3) byte, because that's the way MG does it.
# The methods referred to in the dispatchDict are implemented in message_type.py.
# At construction, the bus name can be given so multiple busses can be read at the same time, assuming disjoint CAN IDs
# Note: we're using can2.0a IDs, but it should also work with can2.0b IDs simultaneously.
class CanbusReceiver(Service):
    """ Note! current charge and current discharge are swapped with respect to the protocol of MG """
    dispatchDict = {
        0x302: {
            0x05: {
                0x20: {
                    0x01: CanbusMessageType.batteryVoltage,
                    0x02: CanbusMessageType.batteryCurrent,
                    0x03: CanbusMessageType.batteryCurrentCharge,
                    0x04: CanbusMessageType.batteryCurrentDischarge,
                    0x05: CanbusMessageType.batterySOC,
                    0x07: CanbusMessageType.batteryTimeToGo
                }
            }
        },
        0x402: {
            0x05: {
                0x20: {
                    0x09: CanbusMessageType.batteryCellTempHigh,
                    0x0b: CanbusMessageType.batteryCellTempLow,
                    0x0c: CanbusMessageType.batteryCellVoltageHigh,
                    0x0d: CanbusMessageType.batteryCellVoltageLow,
                    0x0e: CanbusMessageType.batteryBmsState
                }
            }
        },
        0x482: {
            0x00: {
                0x20: {
                    0x01: CanbusMessageType.batteryCellVoltage,
                    0x02: CanbusMessageType.batteryCellVoltage,
                    0x03: CanbusMessageType.batteryCellVoltage,
                    0x04: CanbusMessageType.batteryCellVoltage,
                    0x05: CanbusMessageType.batteryCellVoltage,
                    0x06: CanbusMessageType.batteryCellVoltage,
                    0x07: CanbusMessageType.batteryCellVoltage,
                    0x08: CanbusMessageType.batteryCellVoltage,
                    0x09: CanbusMessageType.batteryCellVoltage,
                    0x0a: CanbusMessageType.batteryCellVoltage,
                    0x0b: CanbusMessageType.batteryCellVoltage,
                    0x0c: CanbusMessageType.batteryCellVoltage
                }
            }
        },
        0xC9: CanbusMessageType.throttle,

        0xCA: CanbusMessageType.MGMPacket1,
        0xCB: CanbusMessageType.MGMPacket2,
        0xCC: CanbusMessageType.MGMPacket3,
        0xCD: CanbusMessageType.MGMPacket4,

        0x190: CanbusMessageType.XsensErrors,
        0x191: CanbusMessageType.XsensEuler,
        0x192: CanbusMessageType.XsensLatLon,
        0x193: CanbusMessageType.XsensAltitude,
        0x194: CanbusMessageType.XsensVelocity,

        0x76C: CanbusMessageType.MpptCurrentVoltage,
        0x76D: CanbusMessageType.MpptPowerTemperature,
        0x76E: CanbusMessageType.MpptCurrentVoltage,
        0x76F: CanbusMessageType.MpptPowerTemperature,
        0x770: CanbusMessageType.MpptCurrentVoltage,
        0x771: CanbusMessageType.MpptPowerTemperature,
        0x772: CanbusMessageType.MpptCurrentVoltage,
        0x773: CanbusMessageType.MpptPowerTemperature,
        0x774: CanbusMessageType.MpptCurrentVoltage,
        0x775: CanbusMessageType.MpptPowerTemperature,
        0x776: CanbusMessageType.MpptCurrentVoltage,
        0x777: CanbusMessageType.MpptPowerTemperature,
        0x778: CanbusMessageType.MpptCurrentVoltage,
        0x779: CanbusMessageType.MpptPowerTemperature,
        0x77A: CanbusMessageType.MpptCurrentVoltage,
        0x77B: CanbusMessageType.MpptPowerTemperature,
        0x77C: CanbusMessageType.MpptCurrentVoltage,
        0x77D: CanbusMessageType.MpptPowerTemperature,
        0x77E: CanbusMessageType.MpptCurrentVoltage,
        0x77F: CanbusMessageType.MpptPowerTemperature,
        0x780: CanbusMessageType.MpptCurrentVoltage,
        0x781: CanbusMessageType.MpptPowerTemperature,
        0x782: CanbusMessageType.MpptCurrentVoltage,
        0x783: CanbusMessageType.MpptPowerTemperature,
    }

    def __init__(self, vessel, bus = 'can0'):
        super().__init__(vessel)
        self.bus = bus

        try:
            check_call(['sudo', 'ifconfig', bus, 'down'])
            check_call(['sudo', 'ip', 'link', 'set', bus, 'type', 'can', 'bitrate', '250000']) # 250000 is bitrate (250kbps)
            check_call(['sudo', 'ifconfig', bus, 'up'])
        except Exception as e:
            logging.error("Could not start canbus_receiver: " + str(e))
            self.stopped = True

    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                canbus = can.interface.Bus(channel = self.bus, bustype = 'socketcan')
                while True:
                    if self.isStopped(): return
                    msg = canbus.recv(10.0)
                    self.dispatch(msg)
            except Exception as e:
                logging.info('Exception with CAN bus (' + self.bus +'). Retry in 2 seconds. ' + str(e))
                time.sleep(2)

    def dispatch(self, msg):
        try:
            if(callable(self.dispatchDict[msg.arbitration_id])):
                self.dispatchDict[msg.arbitration_id](msg, self.storage)
            else:
                self.dispatchDict[msg.arbitration_id][msg.data[1]][msg.data[2]][msg.data[3]](msg, self.storage)
        except:
            pass