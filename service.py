class Service:
    def __init__(self, vessel):
        self.vessel = vessel
        self.storage = self.vessel.storage
        self.stopped = False
        
    def run(self):
        self.stopped = False

    def kill(self):
        self.stopped = True

    def isStopped(self):
        return self.stopped if self.stopped is not None else False