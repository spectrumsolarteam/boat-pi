import time
import datetime
import logging
import jwt
import paho.mqtt.client as mqtt
import json
from service import Service

class MqttTransceiver(Service):
    connection = None

    ssl_private_key_filepath = '/home/pi/keys/rsa.private'
    ssl_algorithm = 'RS256'
    root_cert_filepath = '/home/pi/keys/google-roots.pem'
    project_id = 'cetacea-solar'
    gcp_location = 'europe-west1'
    registry_id = 'cetacea-v1'
    device_id = 'spectrum-apparaat'

    token_expiration = 72000        # 72000 seconds is 20 hours
    timestamp_connected = None

    def __init__(self, vessel):
        super().__init__(vessel)
        self.CLIENT_ID = 'projects/{}/locations/{}/registries/{}/devices/{}'.format(self.project_id, self.gcp_location, self.registry_id, self.device_id)
        self.MQTT_PUBLISH_TOPIC = '/devices/{}/events'.format(self.device_id)
        self.MQTT_SUBSCRIBE_TOPIC = '/devices/{}/commands/#'.format(self.device_id)

    def run(self):
        super().run()
        while True:
            if self.isStopped(): return
            try:
                logging.info('(Re)try in the MQTT thead')
                self.client = mqtt.Client(client_id=self.CLIENT_ID)
                self.client.username_pw_set(username = 'unused', password = self.create_jwt())

                self.client.on_connect = self.on_connect
                self.client.on_disconnect = self.on_disconnect
                self.client.on_message = self.on_message
                self.client.suppress_exceptions = False
                # self.client.on_publish = self.on_publish

                self.client.tls_set(ca_certs = self.root_cert_filepath)
                self.client.connect('mqtt.googleapis.com', 8883)
                self.client.loop_start()
                self.timestamp_connected = time.time()

                self.connection = self.client

                exceptionCount = 0

                while True:
                    if self.isStopped(): return
                    try:
                        t = time.time()

                        payload = self.storage.toJson(True)
                        self.client.publish(self.MQTT_PUBLISH_TOPIC, payload, qos=1)
                        try:
                            logging.info('MQQT_PUBLISH with timestamp {}'.format(json.loads(payload)['epoch']))
                        except Exception as ex:
                            pass

                        if t > (self.timestamp_connected + self.token_expiration):
                            logging.info('Woah, the MQTT token has expired!')
                            break

                        sleeptime = (int(t) + 1.2) - time.time()
                        time.sleep(0 if sleeptime < 0 else sleeptime)

                        exceptionCount = 0
                    except Exception as ex:
                        if exceptionCount >= 3:
                            raise
                        
                        logging.info('Small exception in MQTT thread (A). Sleep for 1 second' + str(ex))
                        exceptionCount += 1
                        time.sleep(1)

                #logging.info('Out of 2nd while loop')
                self.client.loop_stop()

            except Exception as ex:
                try:
                    self.client.loop_stop()
                except:
                    pass
                print('Exception in MQTT thread (B). Retrying in 5 seconds...', ex)
                logging.info('Exception in MQTT thread. Retrying in 5 seconds... ' + str(ex))
                time.sleep(5)

    def create_jwt(self):
        cur_time = datetime.datetime.utcnow()

        token = {
            'iat': cur_time,
            'exp': cur_time + datetime.timedelta(seconds=self.token_expiration),
            'aud': self.project_id
        }

        with open(self.ssl_private_key_filepath, 'r') as f:
            private_key = f.read()

        return jwt.encode(token, private_key, self.ssl_algorithm)

    def error_str(self, rc):
        return 'error string: ' + rc

    def on_connect(self, client, userdata, flags, rc):
        # The value of rc determines success or not:
        # 0: Connection successful
        # 1: Connection refused - incorrect protocol version
        # 2: Connection refused - invalid client identifier
        # 3: Connection refused - server unavailable
        # 4: Connection refused - bad username or password
        # 5: Connection refused - not authorised

        if(rc == 0):
            # Subscribing (to topic) in on_connect() means that if we lose the connection and reconnect then subscriptions will be renewed.
            logging.info('MQTT connected succesfully!')
            client.subscribe(self.MQTT_SUBSCRIBE_TOPIC)
        else:
            logging.info('Tried to connect but didn\'t succeed')

    def on_disconnect(self, client, userdata, rc):
        logging.info('MQTT disconnected :(')

    def on_publish(self, unused_client, unused_userdata, unused_mid):
        pass
    
    def on_message(self, client, userdata, msg):
        # Expected message format: "attribute_name:value", e.g.: "indigo_coolant_pump:true"
        try:
            message = str(msg.payload.decode('utf-8'))
            if message == 'system:update':
                self.vessel.update()
            if message.startswith('indigo_coolant_pump:'):
                # todo: send canbus message to turn the pump on
                pass
            if message.startswith('indigo_bilge_pump:'):
                # todo: send canbus message to turn the pump on
                pass
        except Exception as e:
            logging.info('Exception in MQTT on_message: ' + str(e))